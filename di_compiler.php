<?php
/**
 * Скрипт сборки di-контейнера
 * 
 * usage:
 * php di_compiler.php mamba_dir_path config_path compiled_file_path_tpl environment
 * 
 * example:
 * php di_compiler.php ~/mamba .configuration/services.php .compiled/%s production
 */

use Mamba\ObjectMapper\Metadata\Cache\MetadataCacheGenerator;
use Mamba\ObjectMapper\Metadata\ConfigBasedMetadataProvider;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DependencyInjection\Compiler\CheckExceptionOnInvalidReferenceBehaviorPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Dumper\PhpDumper;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;

error_reporting(-1);
ini_set('display_errors', 1);

// считаем что любая ошибка в процессе сборки это веская причина для прерывания процесса
set_error_handler(
    function ($errSeverity, $errMsg, $errFile, $errLine) {
        // Тупой нотис, который умышленно создаётся в PHPParser.
        // Не хочется из-за этого отключать обработку всех нотисов, поэтому ловим и игнорим.
        if ($errSeverity === E_NOTICE && $errMsg === 'Undefined variable: undefinedVariable') {
            return false;
        }

        throw new ErrorException($errMsg, 0, $errSeverity, $errFile, $errLine);
    },
    E_ALL
);
    
register_shutdown_function(
    function () {
        $error = error_get_last();
        if ((int)$error["type"] & (E_ERROR | E_COMPILE_ERROR | E_CORE_ERROR | E_ERROR | E_USER_ERROR)) {
            echo("Fatal error: {$error['message']} in {$error['file']} on line {$error['line']}");
        }
    }
);

// первым аргументом надо указать путь до мамбы
if (empty($_SERVER['argv'][1]) || !is_dir($_SERVER['argv'][1])) {
    throw new InvalidArgumentException('Please specify the path to the mamba project as the first argument');
}

$autoloaderPath = rtrim($_SERVER['argv'][1], '/') . '/autoloader.php';

if (!file_exists($autoloaderPath)) {
    throw new InvalidArgumentException('Incorrect path to the mamba project');
}

require $autoloaderPath;
require PHPWEB_PATH_CONFIGURATION . 'sql.inc'; // если не подрубить эти константы, то в процессе сборки будут ошибки

$inputDefinition = new InputDefinition([
    new InputArgument('mamba_dir_path', InputArgument::REQUIRED),
    new InputArgument('config_path', InputArgument::REQUIRED),
    new InputArgument('compiled_file_path', InputArgument::REQUIRED),
    new InputArgument('environment', InputArgument::REQUIRED),
    
    new InputOption('compiled_class', 'c', InputOption::VALUE_REQUIRED, '', 'DiContainer'),
    new InputOption('do_not_optimize', 'o', InputOption::VALUE_OPTIONAL, '', '0'),
]);

$output = new ConsoleOutput();

try {
    $input = new ArgvInput($_SERVER['argv'], $inputDefinition);
    $environment = strtolower($input->getArgument('environment'));
    
    if ($environment == 'production') {
        define('PHPWEB_MODE', 1);
    } else if ($environment == 'devel') {
        define('PHPWEB_MODE', 2);
    } else {
        throw new InvalidArgumentException('Invalid environment value - ' . $environment);
    }

    define('PHPWEB_CONFIG_PATH', $environment);
    define('PHPWEB_PATH_CONFIG_CURRENT', PHPWEB_PATH_CONFIGURATION . $environment . '/');
    
    // ... иначе не будут работать конфиги
    Core_Settings::setConfigPath(PHPWEB_PATH_CONFIG_CURRENT);
    
    $configFilePath = $input->getArgument('config_path');
    if ($configFilePath[0] !== '/') {
        $configFilePath = PHPWEB_PATH_SYSTEM . $configFilePath;
    }
    
    $pathTemplate = $input->getArgument('compiled_file_path');

    $dicFilepath = sprintf($pathTemplate, 'dic.php');
    $servicesMapFilepath = sprintf($pathTemplate, 'services_map.php');
    $jamrRelationsFilepath = sprintf($pathTemplate, 'jamr_relations.php');
    $servibaraActionsFilepath = sprintf($pathTemplate, 'servibara_actions.php');
    $serializerMetadataCacheFilepath = sprintf($pathTemplate, 'metadata.php');

    $dicFilepathAbs = $dicFilepath[0] === '/'
        ? $dicFilepath
        : PHPWEB_PATH_SYSTEM . $dicFilepath;
    $servicesMapFilepathAbs = $servicesMapFilepath[0] === '/'
        ? $servicesMapFilepath
        : PHPWEB_PATH_SYSTEM . $servicesMapFilepath;
    $jamrRelationsFilepathAbs = $jamrRelationsFilepath[0] === '/'
        ? $jamrRelationsFilepath
        : PHPWEB_PATH_SYSTEM . $jamrRelationsFilepath;
    $servibaraActionsFilepathAbs = $servibaraActionsFilepath[0] === '/'
        ? $servibaraActionsFilepath
        : PHPWEB_PATH_SYSTEM . $servibaraActionsFilepath;
    $serializerMetadataCacheFilepathAbs = $serializerMetadataCacheFilepath[0] === '/'
        ? $serializerMetadataCacheFilepath
        : PHPWEB_PATH_SYSTEM . $serializerMetadataCacheFilepath;

    $output->writeln(
        sprintf(
            '<comment>%s => %s, %s, %s, %s, %s</comment>',
            $configFilePath,
            $dicFilepath,
            $servicesMapFilepath,
            $jamrRelationsFilepath,
            $servibaraActionsFilepath,
            $serializerMetadataCacheFilepath
        )
    );

    // собираем $containerBuilder
    $fileLocator = new FileLocator();
    $containerBuilder = new ContainerBuilder();

    $containerBuilder->setParameter('qapibara.services_map_filepath', $servicesMapFilepath);
    $containerBuilder->setParameter('jamr.relations_filepath', $jamrRelationsFilepath);
    $containerBuilder->setParameter('servibara.actions_filepath', $servibaraActionsFilepath);

    $loader = new PhpFileLoader($containerBuilder, $fileLocator);
    $loader->load($configFilePath);

    // компиляция в рантайме, уберем оптимизирующие компилер-пасы
    if ($input->getOption('do_not_optimize') !== '0') {
        $removingPasses = array(
            new CheckExceptionOnInvalidReferenceBehaviorPass(),
        );

        $containerBuilder->getCompiler()->getPassConfig()->setRemovingPasses($removingPasses);
    }

    $containerBuilder->compile();

    // делаем дамп и записываем его в файл
    $dumper = new PhpDumper($containerBuilder);
    $dump = $dumper->dump(['class' => $input->getOption('compiled_class')]);
    $comment = "/**\n * ATTENTION!\n * I'M AUTO GENERATED FILE!\n * PLEASE, DON'T TOUCH ME!\n */";
    $dump = str_replace("<?php\n", "<?php\n" . $comment . "\n// @codingStandardsIgnoreStart\n", $dump);

    $fileAlreadyExist = file_exists($dicFilepathAbs);
    file_put_contents($dicFilepathAbs, $dump);
    if (!$fileAlreadyExist) {
        chmod($dicFilepathAbs, 0666);
    }

    $phpFilePrefix = "<?php // @codingStandardsIgnoreStart\nreturn ";

    // Qapibara services map
    $serviceMapBuilder = new \Qapibara\Preprocessor\ServiceMapBuilder(
        $containerBuilder->getParameter('api_services_classes'),
        $containerBuilder->getParameter('api_routes_map'),
        $containerBuilder->getParameter('api_watchdogs'),
        $containerBuilder->getParameter('api_model_names')
    );
    $servicesMap = $serviceMapBuilder->buildMap();
    $fileAlreadyExist = file_exists($servicesMapFilepathAbs);
    file_put_contents($servicesMapFilepathAbs, $phpFilePrefix . var_export($servicesMap, true) . ';');
    if (!$fileAlreadyExist) {
        chmod($servicesMapFilepathAbs, 0666);
    }

    // JAMR relations config
    $configPreprocessor = new \Jamr\ConfigPreprocessor();
    $config = require(PHPWEB_PATH_CONFIGURATION . 'common/api/jamr_relations.php');
    $config = $configPreprocessor->expandArrays($config);
    $config = $configPreprocessor->expandRels($config);
    $config = $configPreprocessor->addServiceInfo(
        $config,
        $servicesMap,
        $containerBuilder->getParameter('api_model_names')
    );
    $fileAlreadyExist = file_exists($jamrRelationsFilepathAbs);
    file_put_contents($jamrRelationsFilepathAbs, $phpFilePrefix . var_export($config, true) . ';');
    if (!$jamrRelationsFilepathAbs) {
        chmod($jamrRelationsFilepathAbs, 0666);
    }

    // Servibara actions info
    $actionsInfoExtractor = new \Servibara\Preprocessor\ActionsInfoExtractor($servicesMap);
    $actionsInfo = $actionsInfoExtractor->extractActionsInfo();
    $fileAlreadyExist = file_exists($servibaraActionsFilepathAbs);
    file_put_contents($servibaraActionsFilepathAbs, $phpFilePrefix . var_export($actionsInfo, true) . ';');
    if ($fileAlreadyExist) {
        chmod($servibaraActionsFilepathAbs, 0666);
    }

    $serializerMetadataCacheGenerator = new MetadataCacheGenerator(new ConfigBasedMetadataProvider());
    $fileAlreadyExist = file_exists($serializerMetadataCacheFilepathAbs);
    file_put_contents(
        $serializerMetadataCacheFilepathAbs,
        $serializerMetadataCacheGenerator->generate(PHPWEB_PATH_CONFIGURATION . 'meta')
    );
    if (!$fileAlreadyExist) {
        chmod($serializerMetadataCacheFilepathAbs, 0666);
    }

    $output->writeln('<info>success</info>');
} catch (\Exception $e) {
    $output->writeln(sprintf('<error>%s</error>', $e));
    exit(1);
}
