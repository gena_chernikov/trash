#!/monamour/php/bin/php
<?php

/*

Примеры использования:

$cmd = "curl '{$client->getApiUrl()}/settings/personal/edit/?sid={$client->getLastSid()}&fakeIp=Berlin'";
$cmd = "curl -X POST '{$client->getApiUrl()}/rating/pairs/form/?sid={$client->getLastSid()}'";
$cmd = "curl '{$client->getApiUrl()}/products/gift/?sid={$client->getLastSid()}'
    . '&anketaId=1282286038&tag=gift&langId=en&dateType=timestamp'";
$cmd = "curl '{$client->getApiUrl()}/products/vip/?sid={$client->getLastSid()}'
    . '&anketaId=1282286038&tag=gift&langId=en&dateType=timestamp'";
$cmd = <<<HEREDOC
curl -X POST -d '{
    "product": { "id": "375", "type": "gift" },
    "method":  { "type": "account" },
    "anketaId": "1282286038",
    "sid": "{$client->getLastSid()}",
    "langId": "en",
    "dateType": "timestamp"
}' '{$client->getApiUrl()}/purchase/?sid={$client->getLastSid()}'
HEREDOC;

$cmd = <<<HEREDOC
curl -X POST -d '{ "registration_confirm":
{"confirm":{"phone":"+905068736725", "password": "{$client->getPassword()}" }},
"sid": "{$sid}","langId": "en", "dateType": "timestamp" }' '{$client->getApiUrl()}/registration/confirm/?sid={$sid}'
HEREDOC;

$cmd = <<<HEREDOC
curl -X POST -d '{ "albumId": "1316602821", "sid": "{$sid}", "langId": "en", "dateType": "timestamp" }' \
'{$client->getApiUrl()}/photos/upload/?sid={$sid}'
HEREDOC;
$res = $client->exec($cmd);

// Отправка файла
for ($i = 0; $i < 45; $i++) {
    $res = $client->exec("curl -i -F 'image=@/home/gch/nature.jpg' \
'{$client->getApiUrl()}/photos/upload/?sid={$sid}&albumId=1316602821'");
    echo ".";
}

$res = $client->exec($cmd);

//$cmd = "curl '{$client->getApi4Url()}/?area=Anketa&langId=ru&partnerId=169746190"
//    . "&userId=1215590627&giftCount=4&placeCode=1&sid={$client->getLastSid()}'"
//    . " -H 'Cookie: XDEBUG_SESSION=PHPSTORM' ";

//$cmd = "curl '{$client->getApi4Url()}/?area=Anketa"
//    . "&userId=1215590627&sid={$client->getLastSid()}'"
//    . " -H 'Cookie: XDEBUG_SESSION=PHPSTORM' ";


*/

//include_once __DIR__ . '/../../mamba2/.initialization/console.inc';

/**
 * Class Client
 */
class Client
{
    const DEBUG_SCREEN = 1;

    private $login = 'behat_testing_gch2@mail.ru';

    private $password = 'qwer1234';

    private $debug = 0;

    private $lastSid = false;
    
    private $urlPrefix = 'http://api.mobile-api.ru.www48.lan/';
    //private $urlPrefix = 'http://api.mobile-api.ru/';

    /**
     * @param array|bool $opts
     */
    public function __construct($opts = false) {
        if (is_array($opts)) {
            $this->debug = $opts['debug'] ?: $opts['debug'];
        }
    }

    /**
     * @return string
     */
    public function getLogin() {
        return $this->login;
    }

    /**
     * @param $login
     * @return mixed
     */
    public function setLogin($login) {
        return $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @param $password
     * @return mixed
     */
    public function setPassword($password) {
        return $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSid() {
        $cmd = "curl '{$this->getApiUrl()}/session'";
        $result = $this->exec($cmd);

        return $result['sid'];
    }

    /**
     * @param string $version
     * @return string
     */
    public function getApiUrl($version = 'v5.1.1.1') {
        return $this->urlPrefix . $version;
    }

    /**
     * @param string $version
     * @return string
     */
    public function getApi4Url($version = 'v4.1.10.0') {
        $version = 'v4.2.24.0';
        return $this->urlPrefix . $version;
        //return 'http://api.mobile-api.ru.www48.lan/v5.1.10.0';
        //return 'http://api.mobile-api.ru.www48.lan/v4.14.10.0';
    }

    /**
     * @return bool
     */
    public function getLastSid() {
        return $this->lastSid;
    }

    public function login($sid = false) {
        if (!$sid) {
            $sid = $this->getSid();
        }

        $cmd = <<<HEREDOC
curl -X POST -d '{
        "login":
        {"login": "{$this->login}", "password": "{$this->password}"},
        "sid": "{$sid}", "langId": "ru", "dateType": "timestamp"
    }' '{$this->getApiUrl()}/login/builder/'
HEREDOC;
        $result = $this->exec($cmd);

        return $result;
    }

    /**
     * @param $cmd
     * @return mixed
     */
    public function exec($cmd) {
        $cmd = preg_replace("#\\n#s", '', $cmd);
        $this->log("\nCOMMAND:\n" . $cmd);

        $cmd .= ' 2> /dev/null';
        exec($cmd, $output);
        $result = json_decode($output[0], true);

        if (isset($result['sid'])) {
            $this->lastSid = $result['sid'];
        }

        $strOutput = join('', $output);

        $this->log('RESPONSE:');
        $this->log($strOutput);
        return $result;
    }

    /**
     * @param $msg
     */
    private function log($msg) {
        if ($this->debug === self::DEBUG_SCREEN) {

            $json = json_decode($msg, true);
            if (is_array($json)) {
                $prettyText = json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            } else {
                $prettyText = $msg;
            }

            echo $prettyText . "\n";
        }
    }
}


$client = new Client([
    'debug' => Client::DEBUG_SCREEN
]);


$client->setLogin('test.felix@yandex.ru');
$client->setPassword('9bRRMKQJ');

//$client->setLogin('asasd1258@yandex.ru');
//$client->setPassword('qwerty32');

$res = $client->login();
$sid = $res['sid'];

echo $sid . PHP_EOL;


$cmd = "curl '{$client->getApiUrl()}/users/1227399773/albums/photos/?sid={$client->getLastSid()}'";
$client->exec($cmd);
